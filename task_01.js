"use strict";

let users = [];
let usersNumber = parseInt(prompt('Enter number of users'));

function validation(paramName, validLength) {
  let paramValue;
  do {
    if (paramName === "birthday") {
    	paramValue = new Date(prompt(`Enter user's ${paramName} in the format 2019-11-21`)).toLocaleDateString();
    } else {
    	paramValue = prompt(`Enter user's ${paramName} (minimum ${validLength} characters)`);
    }
  } while (typeof paramValue === "string" && paramValue.trim().length < validLength);
  return paramValue;
}

function getUsers(usersNum) {
  let userData = [{
      paramName: "name",
      validLength: 3
    },
    {
      paramName: "surname",
      validLength: 3
    },
    {
      paramName: "birthday",
      validLength: 10
    }
  ];
  let i = 0;
  let parameter;
  for (i; i < usersNum; i++) {
    let user = {};
    for (parameter in userData) {
      let result = validation(userData[parameter].paramName, userData[parameter].validLength);
      if (!result) {
        return alert("Operation was canceled")
      }
      user[userData[parameter].paramName] = result;
    }
    users.push(user);
  }
  return users
}
getUsers(usersNumber);
console.log(users);
